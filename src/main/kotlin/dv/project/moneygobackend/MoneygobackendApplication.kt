package dv.project.moneygobackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MoneygobackendApplication

fun main(args: Array<String>) {
    runApplication<MoneygobackendApplication>(*args)
}
