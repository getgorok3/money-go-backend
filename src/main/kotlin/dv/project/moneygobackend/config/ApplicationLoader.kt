package dv.project.moneygobackend.config

import dv.project.moneygobackend.entity.*
import dv.project.moneygobackend.repository.ImageSetRepository
import dv.project.moneygobackend.repository.TransactionRepository
import dv.project.moneygobackend.repository.UserRepository
import dv.project.moneygobackend.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import java.time.LocalDate
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var walletRepository: WalletRepository

    @Autowired
    lateinit var transactionReposity: TransactionRepository

    @Autowired
    lateinit var imageSetRepository: ImageSetRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var user1 = userRepository.save(
                User("Thanakan@gmail.com", "1234", "Thanakan", "Thanakwang")
        )

        var wallet1 = walletRepository.save(
                Wallet("My first wallet", 500.0)
        )

        var wallet2 = walletRepository.save(
                Wallet("My Fav wallet", 1000.00)
        )

//        var image1 = imageSetRepository.save(
//                ImageSet(url = "http://www.chiangmaiexpert.com/wp-content/uploads/2016/05/page-1170x800.jpg")
//        )
//        var image2 = imageSetRepository.save(
//                ImageSet(url = "https://readthecloud.co/wp-content/uploads/2018/10/MG_7641-62.jpg")
//        )


        var transaction1 = transactionReposity.save(
                Transaction(topic = "Yummy", type = TransactionType.EXPENSE, category = TransactionCategory.FOOD,
                        date = LocalDate.parse("2019-03-21"), cash = 100.0
                )
        )
        var transaction2 = transactionReposity.save(
                Transaction(topic = "Delicious meal", type = TransactionType.EXPENSE, category = TransactionCategory.FOOD,
                        date = LocalDate.parse("2019-03-22"), cash = 250.0
                )
        )



        user1.wallets.add(wallet1)
        user1.wallets.add(wallet2)

        transaction1.imageSet="http://www.chiangmaiexpert.com/wp-content/uploads/2016/05/page-1170x800.jpg"
        transaction2.imageSet="https://readthecloud.co/wp-content/uploads/2018/10/MG_7641-62.jpg"

        wallet1.transactions.add(transaction1)
        wallet1.transactions.add(transaction2)



    }
}