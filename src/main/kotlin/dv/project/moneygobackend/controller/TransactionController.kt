package dv.project.moneygobackend.controller


import dv.project.moneygobackend.entity.dto.TransactionDto
import dv.project.moneygobackend.service.TransactionService
import dv.project.moneygobackend.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
class TransactionController {

    @Autowired
    lateinit var transactionService: TransactionService

    @GetMapping("/transactions")
    fun getAllTransactions(): ResponseEntity<Any> {
        val output = transactionService.getAllTransactions()
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapTransactionDto(output))

    }

    @GetMapping("/transactions/user/{userid}/date/{date}")
    fun getAllTransactionSortByDate(
            @PathVariable("userid") userid: Long
            , @PathVariable("date") date: String): ResponseEntity<Any> {
        val output = transactionService.getAllTransactionSortByDate(userid, date)
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapTransactionDto(output))
    }

    @PostMapping("/transaction/add/{walletId}")
    fun addTransaction(
            @PathVariable("walletId") walletId: Long,
            @RequestBody transaction: TransactionDto): ResponseEntity<Any> {
        val output = transactionService.save(walletId, MapperUtil.INSTANCE.mapTransactionDto(transaction))
        val outputDto =MapperUtil.INSTANCE.mapTransactionDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()

    }


}