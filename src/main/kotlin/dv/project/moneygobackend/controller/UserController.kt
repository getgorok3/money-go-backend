package dv.project.moneygobackend.controller

import dv.project.moneygobackend.entity.dto.UserDto
import dv.project.moneygobackend.service.UserService
import dv.project.moneygobackend.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserController {

    @Autowired
    lateinit var userService: UserService

    @GetMapping("/users")
    fun getAllUser(): ResponseEntity<Any> {
        val output = userService.getAllUsers()
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapUserDto(output))

    }
    @GetMapping("/users//{username}/pass/{passW}")
    fun getAUser(@PathVariable("username") username: String,
                 @PathVariable("passW") passW: String): ResponseEntity<Any> {
        val output = userService.getAUsers(username,passW)
        val outputDto =MapperUtil.INSTANCE.mapUserDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

    }
    @GetMapping("/users/id/{userId}")
    fun getUserwithId(
            @PathVariable("userId") userId: Long
    ): ResponseEntity<Any> {
        val output = userService.getUserById(userId)
        val outputDto =MapperUtil.INSTANCE.mapUserDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

    }
    @PostMapping("/users/add/")
    fun addUser(
            @RequestBody user: UserDto): ResponseEntity<Any> {
        val output = userService.save( MapperUtil.INSTANCE.mapUserDto(user))
        val outputDto =MapperUtil.INSTANCE.mapUserDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()

    }



}