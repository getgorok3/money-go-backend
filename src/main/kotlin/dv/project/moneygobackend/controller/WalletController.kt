package dv.project.moneygobackend.controller

import dv.project.moneygobackend.entity.dto.WalletDto
import dv.project.moneygobackend.service.WalletService
import dv.project.moneygobackend.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class WalletController {

    @Autowired
    lateinit var walletService: WalletService

    @GetMapping("/wallets")
    fun getAllWallet(): ResponseEntity<Any> {
        val output = walletService.getAllWallets()
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapWalletDto(output))
    }

    @GetMapping("/wallets/userID/{userId}")
    fun getAllWalletsByUserId(
            @PathVariable userId: Long): ResponseEntity<Any> {
        val output = walletService.getAllWalletsByUserId(userId)
        return ResponseEntity.ok(output)        // map manual
    }


    @PostMapping("/wallets/add/{userId}")
    fun addWalletsByUserId(
            @PathVariable("userId") userId: Long,
            @RequestBody wallet: WalletDto): ResponseEntity<Any> {
        val output = walletService.save(userId, MapperUtil.INSTANCE.mapWalletDto(wallet))
        val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()

    }

    @DeleteMapping("/wallets/id/{walletId}")
    fun deleteWallet(@PathVariable("walletId") walletId: Long): ResponseEntity<Any> {
        val wallet = walletService.remove(walletId)
        val outputProduct = MapperUtil.INSTANCE.mapWalletDto(wallet)
        outputProduct?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The product is not found")
    }

    //add more money to this wallet
    @PostMapping("/wallets/editMoney/")
    fun editWalletMoney(
            @RequestBody wallet: WalletDto): ResponseEntity<Any> {
        val output = walletService.editWalletMoney(MapperUtil.INSTANCE.mapWalletDto(wallet))
        val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()

    }


}