package dv.project.moneygobackend.dao

import dv.project.moneygobackend.entity.ImageSet

interface ImageDao{
    fun save(imageSet: ImageSet): ImageSet
}