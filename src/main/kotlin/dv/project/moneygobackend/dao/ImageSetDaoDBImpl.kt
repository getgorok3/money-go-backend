package dv.project.moneygobackend.dao

import dv.project.moneygobackend.entity.ImageSet
import dv.project.moneygobackend.repository.ImageSetRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ImageSetDaoDBImpl: ImageDao{
    override fun save(imageSet: ImageSet): ImageSet {
        return imageSetRepository.save(imageSet)
    }

    @Autowired
    lateinit var imageSetRepository: ImageSetRepository

}