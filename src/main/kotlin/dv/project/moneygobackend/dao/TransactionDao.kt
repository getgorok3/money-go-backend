package dv.project.moneygobackend.dao

import dv.project.moneygobackend.entity.Transaction
import dv.project.moneygobackend.entity.TransactionType
import java.time.LocalDate

interface TransactionDao {
    fun getTransactionById(id: Long): Transaction?
    fun getAlltransactions(): List<Transaction>
    fun getAllTransactionSortByDate(date: String): List<Transaction>
    fun findByTypeAndID(id: Long, type: TransactionType): Transaction?
    fun save(transaction: Transaction): Transaction

}