package dv.project.moneygobackend.dao

import dv.project.moneygobackend.entity.Transaction
import dv.project.moneygobackend.entity.TransactionType
import dv.project.moneygobackend.repository.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Profile("db")
@Repository
class TransactionDaoDBImpl :  TransactionDao{
    override fun save(transaction: Transaction): Transaction {
        return transactionRepository.save(transaction)
    }

    override fun findByTypeAndID(id: Long, type: TransactionType): Transaction? {
        return transactionRepository.findByTypeAndId(id, type)
    }


    override fun getAllTransactionSortByDate(date: String): List<Transaction> {
        val localDate = LocalDate.parse(date)
        return transactionRepository.findByDate(localDate)
    }

    @Autowired
    lateinit var transactionRepository: TransactionRepository

    override fun getTransactionById(id: Long): Transaction? {
        return transactionRepository.findById(id).orElse(null)
    }

    override fun getAlltransactions(): List<Transaction> {
        return transactionRepository.findAll().filterIsInstance(Transaction::class.java)
    }




}