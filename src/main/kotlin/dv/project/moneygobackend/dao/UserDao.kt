package dv.project.moneygobackend.dao

import dv.project.moneygobackend.entity.User

interface UserDao {
    fun getUserById(id: Long): User?
    fun getAllUsers(): List<User>
    fun save(user: User): User
    fun findById(userId: Long): User
     fun findByEmailAndPassWord(username: String, pass: String): User
}