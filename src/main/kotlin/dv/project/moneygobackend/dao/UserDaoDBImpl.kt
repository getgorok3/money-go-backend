package dv.project.moneygobackend.dao

import dv.project.moneygobackend.entity.User
import dv.project.moneygobackend.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDaoDBImpl : UserDao {
    override fun findByEmailAndPassWord(email: String, pass: String): User {
        return userRepository.findByEmailAndPassword(email,pass)
    }

    override fun findById(userId: Long): User {
        return userRepository.findById(userId).orElse(null)
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun getAllUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }

    override fun getUserById(id: Long): User? {
        return userRepository.findById(id).orElse(null)
    }
    @Autowired
    lateinit var userRepository: UserRepository

}