package dv.project.moneygobackend.dao

import dv.project.moneygobackend.entity.Wallet

interface WalletDao {
    fun getWalletById(id: Long): Wallet?
    fun getAllwallets(): List<Wallet>
    fun save(wallet: Wallet): Wallet
}