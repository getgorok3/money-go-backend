package dv.project.moneygobackend.dao

import dv.project.moneygobackend.entity.Wallet
import dv.project.moneygobackend.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class WalletDaoDBImpl : WalletDao {

    override fun save(wallet: Wallet): Wallet {
        return walletRepository.save(wallet)
    }

    override fun getAllwallets(): List<Wallet> {
        return walletRepository.findAll().filterIsInstance(Wallet::class.java)
    }

    override fun getWalletById(id: Long): Wallet? {
        return walletRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var walletRepository: WalletRepository


}