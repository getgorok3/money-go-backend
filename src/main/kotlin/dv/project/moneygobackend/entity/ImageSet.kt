package dv.project.moneygobackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class ImageSet(var url: String? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null
}