package dv.project.moneygobackend.entity

import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class Transaction(var topic: String? = null,
                       var date: LocalDate? = null,
                       var cash: Double? = null,
                       var type: TransactionType? = null,
                       var category: TransactionCategory? = null,
                       var imageSet: String? =null) {
    @Id
    @GeneratedValue
    var id: Long? = null

//    @OneToMany
//    var imageSet = mutableSetOf<ImageSet>()

    fun getRealValue(transaction: Transaction): Double {
        if (transaction.type!! == TransactionType.INCOME) {
            return transaction.cash!!
        } else {
            return -transaction.cash!!
        }
    }

}