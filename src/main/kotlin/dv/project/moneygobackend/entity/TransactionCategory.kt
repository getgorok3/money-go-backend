package dv.project.moneygobackend.entity

enum class TransactionCategory{
    FOOD,
    BILLs,
    TRANSPORTATION,
    HOME,
    ENTERTAINMENT,
    SHOPPHING,
    CLOTHING,
    TAX,
    HEALTH,
    PET,
    BABY,
    SALARY,
    AWARDS,
    SALE,
    RENTAL,
    REFUNDS,
    LOTTERY,
    INVESTMENT,
    GRANTS,
    OTHERS

}