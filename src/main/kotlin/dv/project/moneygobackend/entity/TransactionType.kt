package dv.project.moneygobackend.entity

enum class TransactionType{
    INCOME, EXPENSE
}