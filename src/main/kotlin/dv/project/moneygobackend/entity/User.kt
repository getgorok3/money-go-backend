package dv.project.moneygobackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class User(override var email: String?=null ,
                override var password: String?=null,
                override var firstName: String?=null,
                override var lastName: String?=null,
                override var userImage: String? =null,
                override var userStatus: UserStatus? = UserStatus.PENDING,
                var bankBalance: Double?=0.0
                )  : UserAccount {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var wallets = mutableListOf<Wallet>()








}
