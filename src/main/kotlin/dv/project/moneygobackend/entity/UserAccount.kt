package dv.project.moneygobackend.entity

interface UserAccount{
    var email: String?
    var password: String?
    var firstName: String?
    var lastName: String?
    var userStatus: UserStatus?
    var userImage: String?

}