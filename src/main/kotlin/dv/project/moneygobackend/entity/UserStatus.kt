package dv.project.moneygobackend.entity

enum class UserStatus{
    ACTIVE, DELETED, PENDING
}