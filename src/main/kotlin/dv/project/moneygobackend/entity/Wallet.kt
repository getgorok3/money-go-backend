package dv.project.moneygobackend.entity

import javax.persistence.*

@Entity
data class Wallet(var name: String? = null,
                  var money: Double? = null,
                  var isDeleted: Boolean? = false) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var transactions = mutableListOf<Transaction>()

}