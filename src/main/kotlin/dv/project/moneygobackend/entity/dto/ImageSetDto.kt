package dv.project.moneygobackend.entity.dto

data class ImageSetDto(
        var url: String? =null,
        var id:Long? =null
)