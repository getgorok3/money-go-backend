package dv.project.moneygobackend.entity.dto

import dv.project.moneygobackend.entity.TransactionCategory
import dv.project.moneygobackend.entity.TransactionType
import java.time.LocalDate

data class TransactionDto (var topic: String? = null,
                           var cash: Double? =null,
                           var type: TransactionType? = null,
                           var category: TransactionCategory? = null,
                           var id: Long? =null,
//                           var imageSet: List<ImageSetDto>? = null
                           var imageSet: String? = null)