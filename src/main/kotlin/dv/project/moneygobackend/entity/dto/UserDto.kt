package dv.project.moneygobackend.entity.dto

import dv.project.moneygobackend.entity.UserStatus
import dv.project.moneygobackend.entity.Wallet

data class UserDto(var id: Long? = null,
                   var email: String? = null,
                   var password: String? = null,
                   var firstName: String? = null,
                   var lastName: String? = null,
                   var bankBalance: Double? = 0.0,
                   var userImage: String? = null)