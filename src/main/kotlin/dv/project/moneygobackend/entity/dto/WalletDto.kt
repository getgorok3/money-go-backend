package dv.project.moneygobackend.entity.dto

data class WalletDto(var name: String? = null,
                     var money: Double? = null,
                     var id: Long? =null,
                     var isDeleted: Boolean? = false)