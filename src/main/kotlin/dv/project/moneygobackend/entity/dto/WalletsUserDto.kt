package dv.project.moneygobackend.entity.dto

data class WalletsUserDto(
        var id:Long? = null,
        var name: String?=null,
        var money: Double? = null,            //summary money that belong to user
        var income: Double? = null,                 //Summation of income in transactions
        var expense: Double? = null,                 //Summation of expense in transactions
        var transactions: List<DisplayTransaction>? = null         //All transaction in this wallet

)