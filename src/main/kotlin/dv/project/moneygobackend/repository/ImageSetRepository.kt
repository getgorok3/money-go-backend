package dv.project.moneygobackend.repository

import dv.project.moneygobackend.entity.ImageSet
import org.springframework.data.repository.CrudRepository

interface ImageSetRepository: CrudRepository<ImageSet, Long>