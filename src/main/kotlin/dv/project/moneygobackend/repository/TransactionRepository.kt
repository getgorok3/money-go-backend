package dv.project.moneygobackend.repository

import dv.project.moneygobackend.entity.Transaction
import dv.project.moneygobackend.entity.TransactionType
import org.springframework.data.repository.CrudRepository
import java.time.LocalDate

interface TransactionRepository : CrudRepository<Transaction, Long> {
    fun findByDate(date: LocalDate): List<Transaction>

    fun findByTypeAndId(id: Long, type: TransactionType): Transaction?


}