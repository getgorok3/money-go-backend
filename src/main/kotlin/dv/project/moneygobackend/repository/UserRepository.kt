package dv.project.moneygobackend.repository

import dv.project.moneygobackend.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User, Long> {
     fun findByEmailAndPassword(email: String, pass: String): User

}