package dv.project.moneygobackend.repository

import dv.project.moneygobackend.entity.Wallet
import org.springframework.data.repository.CrudRepository

interface WalletRepository : CrudRepository<Wallet, Long> {

}