package dv.project.moneygobackend.service

import dv.project.moneygobackend.entity.Transaction
import java.time.LocalDate


interface TransactionService {

    fun getAllTransactions(): List<Transaction>
    fun getAllTransactionSortByDate(id: Long, date: String): List<Transaction>
    fun save(walletId: Long, transaction: Transaction): Transaction
}