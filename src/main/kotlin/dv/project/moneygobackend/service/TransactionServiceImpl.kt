package dv.project.moneygobackend.service


import dv.project.moneygobackend.dao.ImageDao
import dv.project.moneygobackend.dao.TransactionDao
import dv.project.moneygobackend.dao.UserDao
import dv.project.moneygobackend.dao.WalletDao
import dv.project.moneygobackend.entity.ImageSet
import dv.project.moneygobackend.entity.Transaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate
import javax.transaction.Transactional


@Service
class TransactionServiceImpl : TransactionService {

    @Transactional
    override fun save(walletId: Long, transaction: Transaction): Transaction {
        val wallet = walletDao.getWalletById(walletId)
        val newTransaction = transactionDao.save(transaction)
        wallet?.transactions?.add(newTransaction)
        newTransaction?.imageSet?.let{
            imageSetDao.save(ImageSet(url = newTransaction.imageSet))
        }



        return newTransaction
    }

    @Transactional
    override fun getAllTransactionSortByDate(id: Long, date: String): List<Transaction> {
        val user = userDao.getUserById(id)

        val transactionDateInDb = transactionDao.getAllTransactionSortByDate(date)
        var i = 0;
        val result = mutableListOf<Transaction>()
        for (eachW in user?.wallets!!) {
            for (eachT in eachW.transactions) {
                if (eachT.date == transactionDateInDb[i].date) {
                    result.add(eachT)
                }
            }
        }
        return result
    }

    @Transactional
    override fun getAllTransactions(): List<Transaction> {
        return transactionDao.getAlltransactions()
    }

    @Autowired
    lateinit var transactionDao: TransactionDao

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var walletDao: WalletDao

    @Autowired
    lateinit var imageSetDao: ImageDao


}