package dv.project.moneygobackend.service

import dv.project.moneygobackend.entity.User

interface UserService {
    fun getAllUsers(): List<User>
    fun save(user: User): User
    fun getUserById(userId: Long): User
    fun getAUsers(username: String, pass: String): User
}