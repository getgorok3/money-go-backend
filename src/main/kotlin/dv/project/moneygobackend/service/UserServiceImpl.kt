package dv.project.moneygobackend.service

import dv.project.moneygobackend.dao.UserDao
import dv.project.moneygobackend.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class UserServiceImpl : UserService {
    override fun getAUsers(username: String, pass: String): User {
        val user = userDao.findByEmailAndPassWord(username,pass)
        return user
    }

    @Transactional
    override fun getUserById(userId: Long): User {
        val user = userDao.findById(userId)
        return user
    }

    @Transactional
    override fun save(user: User): User {
        return userDao.save(user)
    }

    @Transactional
    override fun getAllUsers(): List<User> {
        val users = userDao.getAllUsers()
        return users
    }

    @Autowired
    lateinit var userDao: UserDao


}