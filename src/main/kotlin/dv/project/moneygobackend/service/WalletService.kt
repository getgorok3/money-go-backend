package dv.project.moneygobackend.service

import dv.project.moneygobackend.entity.Wallet
import dv.project.moneygobackend.entity.dto.WalletsUserDto

interface WalletService {

    fun getAllWallets(): List<Wallet>
    fun getAllWalletsByUserId(id: Long): List<WalletsUserDto>
    fun save(userId: Long, wallet: Wallet): Wallet
    fun remove(walletId: Long): Wallet?
    fun editWalletMoney(wallet: Wallet): Wallet?


}