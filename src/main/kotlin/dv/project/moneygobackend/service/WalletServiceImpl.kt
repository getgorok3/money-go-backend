package dv.project.moneygobackend.service

import dv.project.moneygobackend.dao.TransactionDao
import dv.project.moneygobackend.dao.UserDao
import dv.project.moneygobackend.dao.WalletDao
import dv.project.moneygobackend.entity.TransactionType
import dv.project.moneygobackend.entity.User
import dv.project.moneygobackend.entity.Wallet
import dv.project.moneygobackend.entity.dto.WalletsUserDto
import dv.project.moneygobackend.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transaction
import javax.transaction.Transactional

@Service
class WalletServiceImpl : WalletService {

    @Transactional
    override fun editWalletMoney(wallet: Wallet): Wallet? {
        val output = wallet.id?.let { walletDao.getWalletById(it) }
        output?.money = wallet.money?.let { output?.money?.plus(it) }
        return output
    }

    @Transactional
    override fun remove(walletId: Long): Wallet? {
        val wallet = walletDao.getWalletById(walletId)
        wallet?.isDeleted = true
        return wallet
    }

    @Transactional
    override fun save(userId: Long, wallet: Wallet): Wallet {
        val user = userDao.getUserById(userId)
        val newWallet = walletDao.save(wallet)
        user?.wallets?.add(newWallet)
        return newWallet
    }

    @Transactional
    override fun getAllWalletsByUserId(id: Long): List<WalletsUserDto> {
        val user = userDao.getUserById(id)

        var walletsUserDtos = mutableListOf<WalletsUserDto>()
        for (each in user?.wallets!!) {
            var output = WalletsUserDto(id = each.id,name = each.name, money = getEachWalletMoney(user, each),
                    transactions = MapperUtil.INSTANCE.mapDisplayTransaction(each.transactions),
                    income = getEachWalletInCome(each), expense = getEachWalletExpense(each))

            if (each.isDeleted != true) {
                walletsUserDtos.add(output)

            }

        }
        user.bankBalance = getUserBankBalance(user, walletsUserDtos)


        return walletsUserDtos
    }

    @Transactional
    override fun getAllWallets(): List<Wallet> {
        return walletDao.getAllwallets()
    }

    fun getEachWalletInCome(wallet: Wallet): Double {
        var income: Double = 0.0
        for (each in wallet.transactions) {
            var result = each.getRealValue(each)
            if (result >= 0) {
                income = income + result
            }
        }
        return income
    }

    fun getEachWalletExpense(wallet: Wallet): Double {
        var expense: Double = 0.0
        for (each in wallet.transactions) {
            var result = each.getRealValue(each)
            if (result < 0) {
                expense = expense + result
            }
        }
        return expense
    }

    fun getEachWalletMoney(user: User, wallet: Wallet): Double {
        var money = wallet.money
        for (each in wallet.transactions) {
            var result = each.getRealValue(each)
            money = money!! + result
        }
        if (money!! < 0) {
            money = 0.0
        }
        return money
    }

    fun getUserBankBalance(user: User, walletsUserDtos: List<WalletsUserDto>): Double {
        val result = walletsUserDtos.map { walletsUserDto ->
            walletsUserDto.money!!
        }.sumByDouble { it }
        return result
    }


    @Autowired
    lateinit var walletDao: WalletDao

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var transactionDao: TransactionDao

}