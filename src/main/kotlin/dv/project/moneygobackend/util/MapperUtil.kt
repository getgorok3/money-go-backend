package dv.project.moneygobackend.util

import dv.project.moneygobackend.entity.ImageSet
import dv.project.moneygobackend.entity.Transaction
import dv.project.moneygobackend.entity.User
import dv.project.moneygobackend.entity.Wallet
import dv.project.moneygobackend.entity.dto.*
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapDisplayTransaction(transaction: List<Transaction>): List<DisplayTransaction>
    //map transaction
    fun mapTransactionDto(transaction: Transaction?): TransactionDto?
    fun mapTransactionDto(transaction: List<Transaction>): List<TransactionDto>
    @InheritInverseConfiguration
    fun mapTransactionDto(transactionDto: TransactionDto): Transaction

    //map wallet
    fun mapWalletDto(wallet: Wallet?): WalletDto?
    fun mapWalletDto(wallet: List<Wallet>): List<WalletDto>
    @InheritInverseConfiguration
    fun mapWalletDto(walletDto: WalletDto): Wallet

    //map User
    fun mapUserDto(user: User?): UserDto?
    fun mapUserDto(user: List<User>): List<UserDto>
    @InheritInverseConfiguration
    fun mapUserDto(userDto: UserDto): User

    //map imageSet
    fun mapImageSetDto( imageSet: ImageSet?): ImageSetDto?
    fun mapImageSetDto( imageSet: List<ImageSet>): List<ImageSetDto>
}

